<?php include("conecta.php");?>
<?php include("banco-funcoes-graficos.php");


$cont_idade9 = 0;
$cont_idade19 = 0;
$cont_idade29 = 0;
$cont_idade39 = 0;
$cont_idade40 = 0;


$clientes = GRAAF1($conexao);
foreach($clientes as $cliente){
	
	
	
	if($cliente['idade'] < 10 )
		$cont_idade9++;
	
	elseif($cliente['idade']>9 && $cliente['idade']<20)
		$cont_idade19++;
	
	elseif($cliente['idade'] > 19 && $cliente['idade'] < 30)
		$cont_idade29++;
	
	elseif($cliente['idade'] > 29 && $cliente['idade'] < 40)
		$cont_idade39++;
	
	else
		$cont_idade40++;


}



?><head>

 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("43", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Idades", "Quantidade", { role: "style" } ],
        ["0 a 9", <?=$cont_idade9?>, "#b87333"],
        ["10 a 19", <?=$cont_idade19?>, "silver"],
        ["20 a 29", <?=$cont_idade29?>, "gold"],
        ["30 a 39", <?=$cont_idade39?>, "color: #e5e4e2"],
		  ["+ de 40", <?=$cont_idade40?>, "color: gold"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "",
        width: 800,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }
  </script>


<div id="columnchart_values" style="width: 900px; height: 300px;"></div>
	</head>