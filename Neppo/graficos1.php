<?php include("conecta.php");?>
<?php include("banco-funcoes.php");


$cont_feminino = 0;
$cont_masculino = 0;

$clientes = graficos($conexao);
foreach($clientes as $cliente){
	
	
	
	if($cliente['sexo'] == 'F')
		$cont_feminino++;
	
	else
		$cont_masculino++;

}



?>



  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

		function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Sexo', 'Quantidade'],
          ['Feminino',     <?=$cont_feminino?>],
          ['Masculino',      <?=$cont_masculino?>],
        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="piechart" style="width: 500px; height: 500px;"></div>
  </body>
</html>